#include "CppUnitTest.h"
#include "CommonCode.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CommonCodeSourceTEST
{
	TEST_CLASS(ListManagerCCUnitTests)
	{
	public:
		// Test case of adding a single list to the list manager
		TEST_METHOD(AddList1)
		{
			string listName = "Kroger";
			string listDescription = "Weekly shopping list";
			ListManager listMaster;
			listMaster.addList(0, listName, listDescription);
			vector<List> allLists = listMaster.getAllLists();
			Assert::AreEqual(listName, allLists[0].getName());
		}

		// Test case of adding two lists to the list manager
		TEST_METHOD(AddList2)
		{
			string listName1 = "Kroger";
			string listName2 = "Walmart";
			ListManager listMaster;
			listMaster.addList(0, listName1, "");
			listMaster.addList(1, listName2, "");
			vector<List> allLists = listMaster.getAllLists();
			Assert::AreEqual(listName1, allLists[0].getName());
			Assert::AreEqual(listName2, allLists[1].getName());
		}

		// Test case of removing one list from the list manager
		TEST_METHOD(RemoveList1)
		{
			string listName = "Kroger";
			string listDescription = "Weekly shopping list";
			ListManager listMaster;
			listMaster.addList(0, listName, listDescription);
			listMaster.removeList(0);
			vector<List> allLists = listMaster.getAllLists();
			int numberOfLists = allLists.size();
			Assert::AreEqual(numberOfLists, 0);
		}

		// Test case of removing two lists from the list manager
		TEST_METHOD(RemoveList2)
		{
			string listName1 = "Kroger";
			string listName2 = "Walmart";
			ListManager listMaster;
			listMaster.addList(0, listName1, "");
			listMaster.addList(1, listName2, "");
			listMaster.removeList(1);
			vector<List> allLists1 = listMaster.getAllLists();
			Assert::AreEqual(listName1, allLists1[0].getName());
			listMaster.removeList(0);
			vector<List> allLists2 = listMaster.getAllLists();
			int numberOfLists = allLists2.size();
			Assert::AreEqual(numberOfLists, 0);
		}

		// Test case of swapping two lists
		TEST_METHOD(MoveList1)
		{
			string listName1 = "Staples";
			string listName2 = "Kohls";
			ListManager listMaster;
			listMaster.addList(0, listName1, "");
			listMaster.addList(1, listName2, "");
			listMaster.moveList(1, 0);
			vector<List> allLists = listMaster.getAllLists();
			Assert::AreEqual(listName2, allLists[0].getName());
			Assert::AreEqual(listName1, allLists[1].getName());
		}

		// Test case of swapping a few lists
		TEST_METHOD(MoveList2)
		{
			string listName1 = "Walmart";
			string listName2 = "Target";
			string listName3 = "Michaels";
			string listName4 = "Walgreens";
			ListManager listMaster;
			listMaster.addList(0, listName1, "");
			listMaster.addList(1, listName2, "");
			listMaster.addList(2, listName3, "");
			listMaster.addList(3, listName4, "");
			// Swapping lists 1 and 2 - Target, Walmart, Michaels, Walgreens
			listMaster.moveList(1, 0);
			// Moving list 2 to 4th position - Target, Michaels, Walgreens, Walmart
			listMaster.moveList(3, 1);
			vector<List> allLists = listMaster.getAllLists();
			Assert::AreEqual(listName2, allLists[0].getName());
			Assert::AreEqual(listName3, allLists[1].getName());
			Assert::AreEqual(listName4, allLists[2].getName());
			Assert::AreEqual(listName1, allLists[3].getName());

		}

		// Test case of editing list name
		TEST_METHOD(EditList1)
		{
			string originalName = "Kroger";
			string modifiedName = "Walmart";
			string description = "";
			ListManager listMaster;
			listMaster.addList(0, originalName, description);
			listMaster.editList(0, modifiedName, description);
			vector<List> allLists = listMaster.getAllLists();
			Assert::AreEqual(modifiedName, allLists[0].getName());
		}

		// Test case of editing list description
		TEST_METHOD(EditList2)
		{
			string name = "Kroger";
			string originalDescription = "";
			string modifiedDescription = "Weekly shopping list";
			ListManager listMaster;
			listMaster.addList(0, name, originalDescription);
			listMaster.editList(0, name, modifiedDescription);
			vector<List> allLists = listMaster.getAllLists();
			Assert::AreEqual(modifiedDescription, allLists[0].getDescription());
		}

		// Test case of getting the list size
		TEST_METHOD(GetListManagerSize1)
		{
			string listName1 = "Kroger";
			string listName2 = "Walmart";
			ListManager listMaster;
			listMaster.addList(0, listName1, "");
			listMaster.addList(1, listName2, "");
			vector<List> allLists = listMaster.getAllLists();
			Assert::AreEqual(2, listMaster.getSizeMasterList());
		}

		// Another test method of getting the list size
		TEST_METHOD(GetListManagerSize2)
		{
			string listName1 = "Kroger";
			string listName2 = "Walmart";
			ListManager listMaster;
			listMaster.addList(0, listName1, "");
			listMaster.addList(1, listName2, "");
			listMaster.removeList(1);
			vector<List> allLists1 = listMaster.getAllLists();
			Assert::AreEqual(1, listMaster.getSizeMasterList());
		}

		// Test method of getting list address
		TEST_METHOD(GetListAddress1)
		{
			string listName1 = "Staples";
			string listName2 = "Kohls";
			ListManager listMaster;
			listMaster.addList(0, listName1, "");
			listMaster.addList(1, listName2, "");
			List* list1 = listMaster.getListAddress(1);
			Assert::AreEqual(listName2, list1->getName());
		}

		// Another method of getting list address
		TEST_METHOD(GetListAddress2)
		{
			string listName1 = "Staples";
			string listName2 = "Kohls";
			ListManager listMaster;
			listMaster.addList(0, listName1, "");
			listMaster.addList(1, listName2, "");
			listMaster.moveList(0, 1);
			List* list1 = listMaster.getListAddress(1);
			Assert::AreEqual(listName1, list1->getName());
		}
	};
}