#include "CppUnitTest.h"
#include "CommonCode.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CommonCodeSourceTEST
{
	TEST_CLASS(ListCCUnitTests)
	{
	public:
		// Test case of adding a list using the list name constructor
		TEST_METHOD(AddNewList1)
		{
			string name = "Target";
			List testList(name);
			Assert::AreEqual(testList.getName(), name);
		}

		// Test case of adding a list using the list name, list description header
		TEST_METHOD(AddNewList2)
		{
			string name = "Target";
			string description = "Grabbing stuff from Target.";
			List testList(name, description);
			Assert::AreEqual(testList.getName(), name);
			Assert::AreEqual(testList.getDescription(), description);
		}

		// Test case of modifying the list name
		TEST_METHOD(ModifyListName1)
		{
			string originalName = "Target";
			string modifiedName = "Kroger";
			List testList(originalName);
			testList.setName(modifiedName);
			Assert::AreEqual(testList.getName(), modifiedName);
		}

		// Another case method of modifying the list name
		TEST_METHOD(ModifyListName2)
		{
			string originalName = "Staples";
			string modifiedName = "Staples list";
			string description = "Back to school shopping";
			List testList(originalName, description);
			testList.setName(modifiedName);
			Assert::AreEqual(testList.getName(), modifiedName);
		}

		// Test case of modifying the list description
		TEST_METHOD(ModifyListDescription1)
		{
			string name = "Kroger";
			string originalDescription = "Weekly shopping list";
			string modifiedDescription = "Shopping list for the week of August 1st";
			List testList(name, originalDescription);
			testList.setDescription(modifiedDescription);
			Assert::AreEqual(testList.getDescription(), modifiedDescription);
		}

		// Another test case of modifying the list description
		TEST_METHOD(ModifyListDescription2)
		{
			string name = "Publix";
			string description = "Vacation shopping";
			List testList(name);
			testList.setDescription(description);
			Assert::AreEqual(testList.getDescription(), description);
		}

		// Test case of adding an item to a list
		TEST_METHOD(AddItemToList1)
		{
			string name = "Apple";
			string emptyString = "";
			List list = List("Target");
			list.addItem(0, name, emptyString, emptyString);
			vector<Item> items = list.getAllItems();
			Assert::AreEqual(items[0].getName(), name);
		}

		// Test case of adding two items to a list
		TEST_METHOD(AddItemToList2)
		{
			string name1 = "Apple";
			string emptyString = "";
			List list = List("Target");
			list.addItem(0, name1, emptyString, emptyString);
			vector<Item> items1 = list.getAllItems();
			Assert::AreEqual(items1[0].getName(), name1);
			string name2 = "Banana";
			list.addItem(0, name2, emptyString, emptyString);
			vector<Item> items2 = list.getAllItems();
			Assert::AreEqual(items2[0].getName(), name2);
			Assert::AreEqual(items2[1].getName(), name1);

		}

		// Test case of removing the only item from a list
		TEST_METHOD(RemoveItemFromList1)
		{
			string itemName1 = "Apples";
			string itemDescription1 = "Honeycrisp";
			string itemQty1 = "1 dozen";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName1, itemDescription1, itemQty1);
			testList.removeItem(0);
			vector<Item> allItems = testList.getAllItems();
			int numberOfItems = allItems.size();
			Assert::AreEqual(numberOfItems, 0);
		}

		// Test case of removing 1 of 2 items from a list
		TEST_METHOD(RemoveItemFromList2)
		{
			string itemName1 = "Apples";
			string itemDescription1 = "Honeycrisp";
			string itemQty1 = "1 dozen";
			string itemName2 = "Milk";
			string itemDescription2 = "1% lowfat";
			string itemQty2 = "1/2 gal";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName1, itemDescription1, itemQty1);
			testList.addItem(1, itemName2, itemDescription2, itemQty2);
			testList.removeItem(0);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(itemName2, allItems[0].getName());
			Assert::AreEqual(itemDescription2, allItems[0].getDescription());
			Assert::AreEqual(itemQty2, allItems[0].getQty());
		}
		
		// Test case of editing all three attributes of one item
		TEST_METHOD(EditItemInList1)
		{
			string originalItemName = "Bananas";
			string originalItemDescription = "";
			string originalItemQty = "1";
			string newItemName = "Cherries";
			string newItemDescription = "In the produce section";
			string newItemQty = "3";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, originalItemName, originalItemDescription, originalItemQty);
			testList.editItem(0, newItemName, newItemDescription, newItemQty);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(newItemName, allItems[0].getName());
			Assert::AreEqual(newItemDescription, allItems[0].getDescription());
			Assert::AreEqual(newItemQty, allItems[0].getQty());
		}

		// Test case of editing attributes of 2 different items
		TEST_METHOD(EditItemInList2)
		{
			string itemName1 = "Bread";
			string itemDescription1 = "Whole wheat";
			string originalItemQty1 = "2 loaves";
			string modifiedItemQty1 = "1 loaf";
			string itemName2 = "Ground beef";
			string originalItemDescription2 = "";
			string modifiedItemDescription2 = "Lean";
			string itemQty2 = "1 lb";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName1, itemDescription1, originalItemQty1);
			testList.addItem(1, itemName2, originalItemDescription2, itemQty2);
			testList.editItem(0, itemName1, itemDescription1, modifiedItemQty1);
			testList.editItem(1, itemName2, modifiedItemDescription2, itemQty2);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(modifiedItemQty1, allItems[0].getQty());
			Assert::AreEqual(modifiedItemDescription2, allItems[1].getDescription());
		}

		// Test case of swapping two items
		TEST_METHOD(MoveItemInList1)
		{
			string itemName1 = "Skyline";
			string itemDescription1 = "Cans";
			string itemQty1 = "3";
			string itemName2 = "Butter";
			string itemDescription2 = "Salted";
			string itemQty2 = "4 sticks";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName1, itemDescription1, itemQty1);
			testList.addItem(1, itemName2, itemDescription2, itemQty2);
			testList.moveItem(0, 1);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(itemName2, allItems[0].getName());
			Assert::AreEqual(itemDescription2, allItems[0].getDescription());
			Assert::AreEqual(itemQty2, allItems[0].getQty());
			Assert::AreEqual(itemName1, allItems[1].getName());
			Assert::AreEqual(itemDescription1, allItems[1].getDescription());
			Assert::AreEqual(itemQty1, allItems[1].getQty());
		}

		// Test case doing two moves
		TEST_METHOD(MoveItemInList2)
		{
			string itemName1 = "Milk";
			string itemName2 = "Butter";
			string itemName3 = "Yogurt";
			string itemName4 = "Cheese";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName1, "", "");
			testList.addItem(1, itemName2, "", "");
			testList.addItem(2, itemName3, "", "");
			testList.addItem(3, itemName4, "", "");
			// Moving Milk to 3rd in list - Butter, Yogurt, Milk, Cheese
			testList.moveItem(2, 0);
			// Moving Yogurt to 4th in list - Butter, Milk, Cheese, Yogurt
			testList.moveItem(3, 1);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(itemName2, allItems[0].getName());
			Assert::AreEqual(itemName1, allItems[1].getName());
			Assert::AreEqual(itemName4, allItems[2].getName());
			Assert::AreEqual(itemName3, allItems[3].getName());
		}

		// Test case of toggling both items off 
		TEST_METHOD(ToggleCheckItemInList1)
		{
			string itemName1 = "Grapes";
			string itemDescription1 = "";
			string itemQty1 = "2";
			string itemName2 = "Bread";
			string itemDescription2 = "Honey wheat";
			string itemQty2 = "1 loaf";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName1, itemDescription1, itemQty1);
			testList.addItem(1, itemName2, itemDescription2, itemQty2);
			testList.toggleItemCheck(0);
			vector<Item> allItems1 = testList.getAllItems();
			// Ensure that checked item is Grapes, and that it is moved to the beginning of the checked off portion of the list
			Assert::AreEqual(false, allItems1[0].getCheck());
			Assert::AreEqual(itemName2, allItems1[0].getName());
			Assert::AreEqual(true, allItems1[1].getCheck());
			Assert::AreEqual(itemName1, allItems1[1].getName());
			testList.toggleItemCheck(0);
			vector<Item> allItems2 = testList.getAllItems();
			// Ensure that both items are checked, and that the newly checked item "Bread" is at the beginning of the checked off portion of the list
			Assert::AreEqual(true, allItems2[0].getCheck());
			Assert::AreEqual(itemName2, allItems2[0].getName());
			Assert::AreEqual(true, allItems2[0].getCheck());
			Assert::AreEqual(itemName1, allItems2[1].getName());
		}

		// Test case of toggling an item on and off
		TEST_METHOD(ToggleCheckItemInList2)
		{
			string itemName = "Cookies";
			string itemDescription = "Sugar";
			string itemQty = "2 packs";
			string listName = "Walmart";
			List testList(listName);
			testList.addItem(0, itemName, itemDescription, itemQty);
			testList.toggleItemCheck(0);
			vector<Item> allItems1 = testList.getAllItems();
			Assert::AreEqual(true, allItems1[0].getCheck());
			testList.toggleItemCheck(0);
			vector<Item> allItems2 = testList.getAllItems();
			Assert::AreEqual(false, allItems2[0].getCheck());
		}

		// Test case for getting a single item
		TEST_METHOD(GetItemFromList1)
		{
			string itemName = "Roast beef";
			string itemDescription = "Boar's Head";
			string itemQty = "3/4 lb";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName, itemDescription, itemQty);
			Assert::AreEqual(itemName, testList.getItem(0).getName());
			Assert::AreEqual(itemDescription, testList.getItem(0).getDescription());
			Assert::AreEqual(itemQty, testList.getItem(0).getQty());
		}

		// Test case for getting a multiple items
		TEST_METHOD(GetItemFromList2)
		{
			string itemName1 = "Apples";
			string itemName2 = "Bananas";
			string itemName3 = "Cherries";
			string listName = "Publix";
			List testList(listName);
			testList.addItem(0, itemName1, "", "");
			testList.addItem(1, itemName2, "", "");
			testList.addItem(2, itemName3, "", "");
			Assert::AreEqual(itemName1, testList.getItem(0).getName());
			Assert::AreEqual(itemName2, testList.getItem(1).getName());
			Assert::AreEqual(itemName3, testList.getItem(2).getName());
		}

		// Test case for finding the start of the checked portion of the list
		TEST_METHOD(StartCheckedPortion1)
		{
			string itemName1 = "Grapes";
			string itemDescription1 = "";
			string itemQty1 = "2";
			string itemName2 = "Bread";
			string itemDescription2 = "Honey wheat";
			string itemQty2 = "1 loaf";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName1, itemDescription1, itemQty1);
			testList.addItem(1, itemName2, itemDescription2, itemQty2);
			testList.removeItem(0);
			Assert::AreEqual(0, testList.getCheckedPositionStart());
		}

		// Another test case for finding the start of the checked portion of the list
		TEST_METHOD(StartCheckedPortion2)
		{
			string itemName1 = "Grapes";
			string itemDescription1 = "";
			string itemQty1 = "2";
			string itemName2 = "Bread";
			string itemDescription2 = "Honey wheat";
			string itemQty2 = "1 loaf";
			string listName = "Kroger";
			List testList(listName);
			testList.addItem(0, itemName1, itemDescription1, itemQty1);
			testList.addItem(1, itemName2, itemDescription2, itemQty2);
			testList.toggleItemCheck(1);
			Assert::AreEqual(0, testList.getCheckedPositionStart());
		}
	};
}
