#include "CppUnitTest.h"
#include "CommonCode.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CommonCodeSourceTEST
{
	TEST_CLASS(ItemCCUnitTests)
	{
	public:
		// Test case of constructing an item with just the item name
		TEST_METHOD(ConstructorItemName1)
		{
			string name = "Bagels";
			Item testItem(name);
			Assert::AreEqual(testItem.getName(), name);
		}

		// Another test case of constructing an item with just the item name
		TEST_METHOD(ConstructorItemName2)
		{
			string name = "Shredded cheese";
			Item testItem(name);
			Assert::AreEqual(testItem.getName(), name);
		}

		// Test case of constructing an item with the item name, description, and quantity
		TEST_METHOD(ConstructorItemNameDescQty1)
		{
			string name = "Apples";
			string description = "Honeycrisp";
			string qty = "1 dozen";
			Item testItem(name, description, qty);
			Assert::AreEqual(testItem.getName(), name);
			Assert::AreEqual(testItem.getDescription(), description);
			Assert::AreEqual(testItem.getQty(), qty);
		}

		// Another test case of constructing an item with the item name, description, and quantity
		TEST_METHOD(ConstructorItemNameDescQty2) 
		{
			string name = "Bread";
			string description = "Honey wheat";
			string qty = "1/2 loaf";
			Item testItem(name, description, qty);
			Assert::AreEqual(testItem.getName(), name);
			Assert::AreEqual(testItem.getDescription(), description);
			Assert::AreEqual(testItem.getQty(), qty);
		}

		// Test case of modifying the item name
		TEST_METHOD(ModifyItemName1) 
		{
			string originalName = "Apples";
			string modifiedName = "Bananas";
			Item testItem(originalName);
			testItem.setName(modifiedName);
			Assert::AreEqual(testItem.getName(), modifiedName);
		}

		// Another test case of modifying the item name
		TEST_METHOD(ModifyItemName2)
		{
			string originalName = "Cherrios";
			string description = "In the cereal aisle";
			string qty = "2";
			string modifiedName = "Bananas";
			Item testItem(originalName, description, qty);
			testItem.setName(modifiedName);
			Assert::AreEqual(testItem.getName(), modifiedName);
		}

		// Test case of modifying the item description
		TEST_METHOD(ModifyItemDescription1)
		{
			string name = "Grapefruit";
			string originalDescription = "Found in the produce section";
			string modifiedDescription = "Found in the produce section next to the oranges";
			string qty = "";
			Item testItem(name, originalDescription, qty);
			testItem.setDescription(modifiedDescription);
			Assert::AreEqual(testItem.getDescription(), modifiedDescription);
		}

		// Another test case of modifying the item description
		TEST_METHOD(ModifyItemDescription2)
		{
			string name = "Grapefruit";
			string description = "Found near the oranges";
			Item testItem(name);
			testItem.setDescription(description);
			Assert::AreEqual(testItem.getDescription(), description);
		}

		// Test case of modifying the item quantity
		TEST_METHOD(ModifyItemQty1)
		{
			string name = "Strawberries";
			string description = "";
			string originalQty = "1 lb";
			string modifiedQty = "2 lbs";
			Item testItem(name, description, originalQty);
			testItem.setQty(modifiedQty);
			Assert::AreEqual(testItem.getQty(), modifiedQty);
		}
		// Another test case of modifying the item quantity
		TEST_METHOD(ModifyItemQty2)
		{
			string name = "Strawberries";
			string qty = "2 packs";
			Item testItem(name);
			testItem.setQty(qty);
			Assert::AreEqual(testItem.getQty(), qty);
		}

		// Test case of toggling the item check once
		TEST_METHOD(ToggleCheck1) 
		{
			string name = "Avocados";
			Item testItem(name);
			testItem.toggleCheck();
			Assert::IsTrue(testItem.getCheck());
		}

		// Test case of toggling the item check twice
		TEST_METHOD(ToggleCheck2) 
		{
			string name = "Avocados";
			Item testItem(name);
			testItem.toggleCheck();
			testItem.toggleCheck();
			Assert::IsFalse(testItem.getCheck());
		}
	};
}
