#include "CppUnitTest.h"
#include "CommonCode.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CommonCodeSourceTEST
{
	TEST_CLASS(ActionCCUnitTests)
	{
	public:

		TEST_METHOD(AddItem1)
		{
			string name = "Cheese";
			string desc = "Shredded cheddar";
			string qty = "2 cups";
			List testList("Kroger", "");
			AddItem action1(name, desc, qty, testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getName(), name);
			Assert::AreEqual(allItems[0].getDescription(), desc);
			Assert::AreEqual(allItems[0].getQty(), qty);
		}

		TEST_METHOD(AddItem2)
		{
			string name1 = "Ketchup";
			string name2 = "Mustard";
			string name3 = "Pickles";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			AddItem action3(name3, "", "", testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getName(), name1);
			Assert::AreEqual(allItems[1].getName(), name2);
			Assert::AreEqual(allItems[2].getName(), name3);
		}

		TEST_METHOD(UndoAddItem1)
		{
			string name = "Cheese";
			string desc = "Shredded cheddar";
			string qty = "2 cups";
			List testList("Kroger", "");
			AddItem action1(name, desc, qty, testList);
			action1.undo(testList);
			vector<Item> allItems = testList.getAllItems();
			int numItems = allItems.size();
			Assert::AreEqual(0, numItems);
		}

		TEST_METHOD(UndoAddItem2)
		{
			string name1 = "Ketchup";
			string name2 = "Mustard";
			string name3 = "Pickles";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			AddItem action3(name3, "", "", testList);
			action3.undo(testList);
			action2.undo(testList);
			vector<Item> allItems = testList.getAllItems();
			int numItems = allItems.size();
			Assert::AreEqual(1, numItems);
			Assert::AreEqual(allItems[0].getName(), name1);
		}

		TEST_METHOD(DeleteItem1)
		{
			string name = "Potato chips";
			string desc = "Lays";
			string qty = "1 bag";
			List testList("Kroger", "");
			AddItem action1(name, desc, qty, testList);
			RemoveItem action2(0, testList);
			vector<Item> allItems = testList.getAllItems();
			int numItems = allItems.size();
			Assert::AreEqual(0, numItems);
		}

		TEST_METHOD(DeleteItem2)
		{
			string name1 = "Potato chips";
			string name2 = "Tortilla chips";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			RemoveItem action3(0, testList);
			vector<Item> allItems = testList.getAllItems();
			int numItems = allItems.size();
			Assert::AreEqual(1, numItems);
			Assert::AreEqual(allItems[0].getName(), name2);
		}

		TEST_METHOD(UndoDeleteItem1)
		{
			string name = "Potato chips";
			string desc = "Lays";
			string qty = "1 bag";
			List testList("Kroger", "");
			AddItem action1(name, desc, qty, testList);
			RemoveItem action2(0, testList);
			action2.undo(testList);
			vector<Item> allItems = testList.getAllItems();
			int numItems = allItems.size();
			Assert::AreEqual(1, numItems);
			Assert::AreEqual(allItems[0].getName(), name);
			Assert::AreEqual(allItems[0].getDescription(), desc);
			Assert::AreEqual(allItems[0].getQty(), qty);
		}

		TEST_METHOD(UndoDeleteItem2)
		{
			string name1 = "Potato chips";
			string name2 = "Tortilla chips";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			RemoveItem action3(0, testList);
			action3.undo(testList);
			vector<Item> allItems = testList.getAllItems();
			int numItems = allItems.size();
			Assert::AreEqual(2, numItems);
			Assert::AreEqual(allItems[0].getName(), name1);
			Assert::AreEqual(allItems[1].getName(), name2);
		}

		TEST_METHOD(EditItem1)
		{
			string name = "Ground beef";
			string originalDesc = "Lean";
			string modifiedDesc = "";
			string originalQty = "1 lb";
			string modifiedQty = "2 lbs";
			List testList("Kroger", "");
			AddItem action1(name, originalDesc, originalQty, testList);
			EditItem action2(0, name, modifiedDesc, modifiedQty, testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getDescription(), modifiedDesc);
			Assert::AreEqual(allItems[0].getQty(), modifiedQty);
		}

		TEST_METHOD(EditItem2)
		{
			string originalName1 = "Pizza";
			string modifiedName1 = "Frozen pizza";
			string originalName2 = "Pasta";
			string modifiedName2 = "Spaghetti";
			List testList("Kroger", "");
			AddItem action1(originalName1, "", "", testList);
			AddItem action2(originalName2, "", "", testList);
			EditItem action3(0, modifiedName1, "", "", testList);
			EditItem action4(1, modifiedName2, "", "", testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getName(), modifiedName1);
			Assert::AreEqual(allItems[1].getName(), modifiedName2);
		}

		TEST_METHOD(UndoEditItem1)
		{
			string name = "Ground beef";
			string originalDesc = "Lean";
			string modifiedDesc = "";
			string originalQty = "1 lb";
			string modifiedQty = "2 lbs";
			List testList("Kroger", "");
			AddItem action1(name, originalDesc, originalQty, testList);
			EditItem action2(0, name, modifiedDesc, modifiedQty, testList);
			action2.undo(testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getDescription(), originalDesc);
			Assert::AreEqual(allItems[0].getQty(), originalQty);
		}

		TEST_METHOD(UndoEditItem2)
		{
			string originalName1 = "Pizza";
			string modifiedName1 = "Frozen pizza";
			string originalName2 = "Pasta";
			string modifiedName2 = "Spaghetti";
			List testList("Kroger", "");
			AddItem action1(originalName1, "", "", testList);
			AddItem action2(originalName2, "", "", testList);
			EditItem action3(0, modifiedName1, "", "", testList);
			EditItem action4(1, modifiedName2, "", "", testList);
			action4.undo(testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getName(), modifiedName1);
			Assert::AreEqual(allItems[1].getName(), originalName2);
		}

		TEST_METHOD(MoveItem1)
		{
			string name1 = "Strawberries";
			string name2 = "Cherries";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			MoveItem action3(0, 1, testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getName(), name2);
			Assert::AreEqual(allItems[1].getName(), name1);
		}

		TEST_METHOD(MoveItem2)
		{
			string name1 = "Carrots";
			string name2 = "Green beans";
			string name3 = "Potatoes";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			AddItem action3(name3, "", "", testList);
			// Moving potatoes to the front of list - Potatoes, Carrots, Green beans
			MoveItem action4(0, 2, testList);
			// Moving green beans to the front of list - Green beans, Potatoes, Carrots
			MoveItem action5(0, 2, testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getName(), name2);
			Assert::AreEqual(allItems[1].getName(), name3);
			Assert::AreEqual(allItems[2].getName(), name1);
		}

		TEST_METHOD(UndoMoveItem1)
		{
			string name1 = "Strawberries";
			string name2 = "Cherries";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			MoveItem action3(0, 1, testList);
			action3.undo(testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getName(), name1);
			Assert::AreEqual(allItems[1].getName(), name2);
		}

		TEST_METHOD(UndoMoveItem2)
		{
			string name1 = "Carrots";
			string name2 = "Green beans";
			string name3 = "Potatoes";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			AddItem action3(name3, "", "", testList);
			// Moving potatoes to the front of list - Potatoes, Carrots, Green beans
			MoveItem action4(0, 2, testList);
			// Moving green beans to the front of list - Green beans, Potatoes, Carrots
			MoveItem action5(0, 2, testList);
			action5.undo(testList);
			action4.undo(testList);
			// Back to the original order
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getName(), name1);
			Assert::AreEqual(allItems[1].getName(), name2);
			Assert::AreEqual(allItems[2].getName(), name3);
		}

		TEST_METHOD(ToggleCheck1)
		{
			string name1 = "Bread";
			string name2 = "Butter";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			ToggleCheck action3(1, testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getCheck(), false);
			Assert::AreEqual(allItems[1].getCheck(), true);
		}

		TEST_METHOD(ToggleCheck2)
		{
			string name1 = "Milk";
			string name2 = "Butter";
			string name3 = "Cheese";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			AddItem action3(name3, "", "", testList);
			ToggleCheck action4(0, testList);
			// List currently - Butter (unchecked), Cheese (unchecked), Milk (checked)
			ToggleCheck action5(1, testList);
			// Final list - Butter (unchecked), Cheese (checked), Milk (checked)
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getCheck(), false);
			Assert::AreEqual(allItems[1].getCheck(), true);
			Assert::AreEqual(allItems[2].getCheck(), true);
			Assert::AreEqual(allItems[0].getName(), name2);
			Assert::AreEqual(allItems[1].getName(), name3);
			Assert::AreEqual(allItems[2].getName(), name1);
		}

		TEST_METHOD(UndoToggleCheck1)
		{
			string name1 = "Bread";
			string name2 = "Butter";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			ToggleCheck action3(1, testList);
			action3.undo(testList);
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getCheck(), false);
			Assert::AreEqual(allItems[1].getCheck(), false);
		}

		TEST_METHOD(UndoToggleCheck2)
		{
			string name1 = "Milk";
			string name2 = "Butter";
			string name3 = "Cheese";
			List testList("Kroger", "");
			AddItem action1(name1, "", "", testList);
			AddItem action2(name2, "", "", testList);
			AddItem action3(name3, "", "", testList);
			ToggleCheck action4(0, testList);
			// List currently - Butter (unchecked), Cheese (unchecked), Milk (checked)
			ToggleCheck action5(1, testList);
			// Final list - Butter (unchecked), Cheese (checked), Milk (checked)
			action5.undo(testList);
			action4.undo(testList);
			// Back to the initial list
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getCheck(), false);
			Assert::AreEqual(allItems[1].getCheck(), false);
			Assert::AreEqual(allItems[2].getCheck(), false);
			Assert::AreEqual(allItems[0].getName(), name1);
			Assert::AreEqual(allItems[1].getName(), name2);
			Assert::AreEqual(allItems[2].getName(), name3);
		}

		TEST_METHOD(AddList1)
		{
			string name = "Kroger";
			string desc = "Shopping list for week of 8/1";
			ListManager testLM;
			AddList action1(name, desc, testLM);
			vector<List> allLists = testLM.getAllLists();
			Assert::AreEqual(allLists[0].getName(), name);
			Assert::AreEqual(allLists[0].getDescription(), desc);
		}

		TEST_METHOD(AddList2)
		{
			string name1 = "Walmart";
			string name2 = "Kohls";
			ListManager testLM;
			AddList action1(name1, "", testLM);
			AddList action2(name2, "", testLM);
			vector<List> allLists = testLM.getAllLists();
			Assert::AreEqual(allLists[0].getName(), name1);
			Assert::AreEqual(allLists[1].getName(), name2);
		}

		TEST_METHOD(RemoveList1)
		{
			string name = "Kroger";
			string desc = "Shopping list for week of 8/1";
			ListManager testLM;
			AddList action1(name, desc, testLM);
			RemoveList action2(0, testLM);
			vector<List> allLists = testLM.getAllLists();
			int numLists = allLists.size();
			Assert::AreEqual(0, numLists);
		}

		TEST_METHOD(RemoveList2)
		{
			string name1 = "Walmart";
			string name2 = "Kohls";
			ListManager testLM;
			AddList action1(name1, "", testLM);
			AddList action2(name2, "", testLM);
			RemoveList action3(0, testLM);
			vector<List> allLists = testLM.getAllLists();
			int numLists = allLists.size();
			Assert::AreEqual(1, numLists);
			Assert::AreEqual(allLists[0].getName(), name2);
		}

		TEST_METHOD(EditList1)
		{
			string originalName = "Kroger";
			string modifiedName = "Staples";
			ListManager testLM;
			AddList action1(originalName, "", testLM);
			EditList action2(0, modifiedName, "", testLM);
			vector<List> allLists = testLM.getAllLists();
			Assert::AreEqual(allLists[0].getName(), modifiedName);
		}

		TEST_METHOD(EditList2)
		{
			string name = "Walmart";
			string originalDesc = "Shopping list - week of 7/24";
			string modifiedDesc = "Shopping list - week of 8/1";
			ListManager testLM;
			AddList action1(name, originalDesc, testLM);
			EditList action2(0, name, modifiedDesc, testLM);
			vector<List> allLists = testLM.getAllLists();
			Assert::AreEqual(allLists[0].getDescription(), modifiedDesc);
		}

		TEST_METHOD(MoveList1)
		{
			string name1 = "Target";
			string name2 = "Kohls";
			ListManager testLM;
			AddList action1(name1, "", testLM);
			AddList action2(name2, "", testLM);
			MoveList action3(0, 1, testLM);
			vector<List> allLists = testLM.getAllLists();
			Assert::AreEqual(allLists[0].getName(), name2);
			Assert::AreEqual(allLists[1].getName(), name1);
		}

		TEST_METHOD(MoveList2)
		{
			string name1 = "Target";
			string name2 = "Kohls";
			string name3 = "Walmart";
			ListManager testLM;
			AddList action1(name1, "", testLM);
			AddList action2(name2, "", testLM);
			AddList action3(name3, "", testLM);
			// Kohls, Target, Walmart
			MoveList action4(0, 1, testLM);
			// Kohls, Walmart, Target
			MoveList action5(1, 2, testLM);
			vector<List> allLists = testLM.getAllLists();
			Assert::AreEqual(allLists[0].getName(), name2);
			Assert::AreEqual(allLists[1].getName(), name3);
			Assert::AreEqual(allLists[2].getName(), name1);
		}

		TEST_METHOD(ActionStack1)
		{
			string name1 = "Potato chips";
			string name2 = "Tortilla chips";
			List testList("Kroger", "");
			ActionTracker actionStack;
			AddItem * action1 = new AddItem(name1, "", "", testList);
			AddItem * action2 = new AddItem(name2, "", "", testList);
			RemoveItem * action3 = new RemoveItem(0, testList);
			actionStack.pushActionToStack(action1);
			actionStack.pushActionToStack(action2);
			actionStack.pushActionToStack(action3);
			actionStack.popActionFromStack(testList);
			vector<Item> allItems = testList.getAllItems();
			int numItems = allItems.size();
			Assert::AreEqual(2, numItems);
			Assert::AreEqual(allItems[0].getName(), name1);
			Assert::AreEqual(allItems[1].getName(), name2);
			actionStack.clearStack();
		}

		TEST_METHOD(ActionStack2)
		{
			string name1 = "Milk";
			string name2 = "Butter";
			string name3 = "Cheese";
			List testList("Kroger", "");
			ActionTracker actionStack;
			AddItem * action1 = new AddItem(name1, "", "", testList);
			AddItem * action2 = new AddItem(name2, "", "", testList);
			AddItem * action3 = new AddItem(name3, "", "", testList);
			ToggleCheck * action4 = new ToggleCheck(0, testList);
			// List currently - Butter (unchecked), Cheese (unchecked), Milk (checked)
			ToggleCheck * action5 = new ToggleCheck(1, testList);
			// Final list - Butter (unchecked), Cheese (checked), Milk (checked)
			actionStack.pushActionToStack(action1);
			actionStack.pushActionToStack(action2);
			actionStack.pushActionToStack(action3);
			actionStack.pushActionToStack(action4);
			actionStack.pushActionToStack(action5);
			actionStack.popActionFromStack(testList);
			actionStack.popActionFromStack(testList);
			// Back to the initial list
			vector<Item> allItems = testList.getAllItems();
			Assert::AreEqual(allItems[0].getCheck(), false);
			Assert::AreEqual(allItems[1].getCheck(), false);
			Assert::AreEqual(allItems[2].getCheck(), false);
			Assert::AreEqual(allItems[0].getName(), name1);
			Assert::AreEqual(allItems[1].getName(), name2);
			Assert::AreEqual(allItems[2].getName(), name3);
			actionStack.clearStack();
		}
	};
}