#include "CommonCode.h"

ListManager::ListManager() {

}

vector<List> ListManager::getAllLists() {
	return masterList;
}

void ListManager::addList(int listLocation, string listName, string listDescription) {
	List newList(listName, listDescription);
	masterList.insert(masterList.begin() + listLocation, newList);
}

void ListManager::removeList(int listLocation) {
	masterList.erase(masterList.begin() + listLocation);
}

List * ListManager::getListAddress(int listLocation) {
	return &masterList[listLocation];
}

void ListManager::moveList(int to, int from) {
	List listToMove = masterList[from];
	masterList.erase(masterList.begin() + from);
	masterList.insert(masterList.begin() + to, listToMove);
}

void ListManager::editList(int listLocation, string listName, string listDescription) {
	masterList[listLocation].setName(listName);
	masterList[listLocation].setDescription(listDescription);
}

int ListManager::getSizeMasterList() {
	return masterList.size();
}