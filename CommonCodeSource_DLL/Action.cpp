#include "CommonCode.h"

Action::Action() {
	// Empty for now, may need to change this later
}

TrackableAction::TrackableAction() {
	// Empty for now, may need to change this later
}

AddItem::AddItem(string itemName, string itemDescription, string itemQty, List& currentList, int itemLocation) {
	// If itemLocation equals -1, no location was entered and the item will be added at the end of the unchecked portion of the list by default
	if (itemLocation == -1) {
		itemLocation = currentList.getCheckedPositionStart() + 1;
	}
	prevLocation = itemLocation;
	currentList.addItem(itemLocation, itemName, itemDescription, itemQty);
}

void AddItem::undo(List & currentList) {
	currentList.removeItem(prevLocation);
}

RemoveItem::RemoveItem(int itemLocation, List& currentList) {
	Item toDelete = currentList.getItem(itemLocation);
	// Store the location of where it was deleted from, as well as info to reconstruct it in case of undo
	prevName = toDelete.getName();
	prevDescription = toDelete.getDescription();
	prevQty = toDelete.getQty();
	prevCheckStatus = toDelete.getCheck();
	prevLocation = itemLocation;
	// Delete the item from the list
	currentList.removeItem(itemLocation);
}

void RemoveItem::undo(List& currentList) {
	currentList.addItem(prevLocation, prevName, prevDescription, prevQty);
	// Ensure that the item has the same check/uncheck status as before
	if (prevCheckStatus == true) {
		currentList.toggleItemCheck(prevLocation);
	}
}

EditItem::EditItem(int itemLocation, string itemName, string itemDescription, string itemQty, List& currentList) {
	Item toEdit = currentList.getItem(itemLocation);
	// Store the location of the edited item, as well as info to bring it to the previous status in case on undo
	prevName = toEdit.getName();
	prevDescription = toEdit.getDescription();
	prevQty = toEdit.getQty();
	prevLocation = itemLocation;
	// Modify the item
	currentList.editItem(itemLocation, itemName, itemDescription, itemQty);
}

void EditItem::undo(List& currentList) {
	currentList.editItem(prevLocation, prevName, prevDescription, prevQty);
}

ToggleCheck::ToggleCheck(int itemLocation, List& currentList) {
	prevPosition = itemLocation;
	bool prevCheck = currentList.getItem(itemLocation).getCheck();
	currentList.toggleItemCheck(itemLocation);
	if (prevCheck) {
		curPosition = currentList.getCheckedPositionStart();
	}
	else {
		curPosition = currentList.getCheckedPositionStart() + 1;
	}
}

void ToggleCheck::undo(List& currentList) {
	currentList.toggleItemCheck(curPosition);
	currentList.moveItem(prevPosition, curPosition);
}

MoveItem::MoveItem(int to, int from, List& currentList) {
	prevTo = to;
	prevFrom = from;
	currentList.moveItem(to, from);
}

void MoveItem::undo(List& currentList) {
	currentList.moveItem(prevFrom, prevTo);
}

ActionTracker::ActionTracker() {
	// Empty for now, may need to change this later
}

void ActionTracker::pushActionToStack(TrackableAction * toStack) {
	actionStack.insert(actionStack.begin(), toStack);
	// Store a maximum of 24 items on the stack
	if (actionStack.size() > 24) {
		TrackableAction* actionToDelete = actionStack.back();
		actionStack.erase(actionStack.end() - 1);
		delete actionToDelete;
	}
}

void ActionTracker::popActionFromStack(List& currentList) {
	TrackableAction* actionToUndo = actionStack.front();
	actionToUndo->undo(currentList);
	actionStack.erase(actionStack.begin());
	delete actionToUndo;
}

void ActionTracker::clearStack() {
	while (actionStack.size() != 0) {
		TrackableAction * actionToDelete = actionStack.front();
		actionStack.erase(actionStack.begin());
		delete actionToDelete;
	}
}

AddList::AddList(string listName, string listDescription, ListManager& currentListManager, int listLocation) {
	// If itemLocation equals -1, no location was entered and the item will be added at the end of the unchecked portion of the list by default
	if (listLocation == -1) {
		listLocation = currentListManager.getSizeMasterList();
	}
	currentListManager.addList(listLocation, listName, listDescription);
}

RemoveList::RemoveList(int listLocation, ListManager& currentListManager) {
	currentListManager.removeList(listLocation);
}

EditList::EditList(int listLocation, string listName, string listDescription, ListManager& currentListManager) {
	currentListManager.editList(listLocation, listName, listDescription);
}

MoveList::MoveList(int to, int from, ListManager& currentListManager) {
	currentListManager.moveList(to, from);
}