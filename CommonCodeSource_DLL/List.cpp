#include "CommonCode.h"

List::List(std::string listName) {
	name = listName;
	description = "";
	checkedPositionStart = -1;
}

List::List(string listName, string listDescription) {
	name = listName;
	description = listDescription;
	checkedPositionStart = -1;
}

string List::getName() {
	return name;
}

void List::setName(string listName) {
	name = listName;
}

string List::getDescription() {
	return description;
}

void List::setDescription(string listDescription) {
	description = listDescription;
}

vector<Item> List::getAllItems() {
	return items;
}

void List::addItem(int itemLocation, string itemName, string itemDescription, string itemQty) {
	Item newItem(itemName, itemDescription, itemQty);
	items.insert(items.begin() + itemLocation, newItem);
	checkedPositionStart++;
}

void List::removeItem(int itemLocation) {
	Item item = items[itemLocation];
	bool checkValue = item.getCheck();
	items.erase(items.begin() + itemLocation);
	// Should only decrement the position of the start of the checked portion of the list if an unchecked item was deleted
	if (checkValue == false) {
		checkedPositionStart--;
	}
}

Item List::getItem(int itemLocation) {
	return items[itemLocation];
}

void List::moveItem(int to, int from) {
	Item item = items[from];
	items.erase(items.begin() + from);
	items.insert(items.begin() + to, item);
}

void List::editItem(int itemLocation, string name, string description, string qty) {
	items[itemLocation].setName(name);
	items[itemLocation].setDescription(description);
	items[itemLocation].setQty(qty);
}

void List::toggleItemCheck(int itemLocation) {
	// Check/uncheck item
	Item item = items[itemLocation];
	item.toggleCheck();
	// Move to correct position and adjust the position of the checked portion of the list
	if (item.getCheck() == true) {
		items.erase(items.begin() + itemLocation);
		items.insert(items.begin() + checkedPositionStart, item);
		checkedPositionStart--;
	}
	else {
		items.erase(items.begin() + itemLocation);
		items.insert(items.begin() + checkedPositionStart + 1, item);
		checkedPositionStart++;
	}
}

int List::getCheckedPositionStart() {
	return checkedPositionStart;
}