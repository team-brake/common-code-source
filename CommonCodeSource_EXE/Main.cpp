#include "CommonCode.h"
#include <iostream>
#include <iomanip>
using namespace std;

int stringToInt(string s) {
	// Essentially to do exception handling for people who are stupid (me) and keep causing errors by typing characters when they should be typing numbers
	int selectionInt;
	try {
		selectionInt = stoi(s);
	}
	catch (...) {
		throw exception("Invalid characters entered");
	}
	return selectionInt;
}

void printMenuItem() {
	cout << "Item Options:" << endl;
	cout << "0. Switch to list options" << endl;
	cout << "1. Add a new item" << endl;
	cout << "2. Remove an item" << endl;
	cout << "3. Edit an item" << endl;
	cout << "4. Toggle check for an item" << endl;
	cout << "5. Move item" << endl;
	cout << "6. Undo previous action" << endl;
	cout << "7. Exit application" << endl << endl;
}

void printMenuList() {
	cout << "List Options: " << endl;
	cout << "0. Switch to item options" << endl;
	cout << "1. Add a new list" << endl;
	cout << "2. Remove a list" << endl;
	cout << "3. Edit a list" << endl;
	cout << "4. Move a list" << endl;
	cout << "5. Exit application" << endl << endl;
}

void addItem(List& currentList, ActionTracker& currentActionStack) {
	string itemName, itemDescription, itemQty;
	cout << "Enter an item name: ";
	getline(cin, itemName);
	cout << "Enter an item description: ";
	getline(cin, itemDescription);
	cout << "Enter an item quantity: ";
	getline(cin, itemQty);
	try {
		AddItem * itemToAdd = new AddItem(itemName, itemDescription, itemQty, currentList);
		currentActionStack.pushActionToStack(itemToAdd);
	}
	catch (exception& e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to add item" << endl;
	}
}

void removeItem(List& currentList, ActionTracker& currentActionStack) {
	int itemLocation;
	string itemLocationStr;
	cout << "Enter the position of the item to delete: ";
	getline(cin, itemLocationStr);
	try {
		itemLocation = stringToInt(itemLocationStr);
		RemoveItem * itemToRemove = new RemoveItem(itemLocation, currentList);
		currentActionStack.pushActionToStack(itemToRemove);
	}
	catch (exception& e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to remove item" << endl;
	}
}

void editItem(List& currentList, ActionTracker& currentActionStack) {
	int itemLocation;
	string itemLocationStr, itemName, itemDescription, itemQty;
	cout << "Enter the position of the item to edit: ";
	getline(cin, itemLocationStr);
	cout << "Enter a new item name: ";
	getline(cin, itemName);
	cout << "Enter a new item description: ";
	getline(cin, itemDescription);
	cout << "Enter a new item quantity: ";
	getline(cin, itemQty);
	try {
		itemLocation = stringToInt(itemLocationStr);
		EditItem * itemToEdit = new EditItem(itemLocation, itemName, itemDescription, itemQty, currentList);
		currentActionStack.pushActionToStack(itemToEdit);
	}
	catch (exception & e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to edit item" << endl;
	}
}

void toggleCheckItem(List& currentList, ActionTracker& currentActionStack) {
	int itemLocation;
	string itemLocationStr;
	cout << "Enter the position of the item to check/uncheck: ";
	getline(cin, itemLocationStr);
	try {
		itemLocation = stringToInt(itemLocationStr);
		ToggleCheck * itemToToggle = new ToggleCheck(itemLocation, currentList);
		currentActionStack.pushActionToStack(itemToToggle);
	}
	catch (exception & e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to toggle item check" << endl;
	}
}

void moveItem(List& currentList, ActionTracker& currentActionStack) {
	int from, to;
	string fromStr, toStr;
	cout << "Enter the position of the item to move: ";
	getline(cin, fromStr);
	cout << "Enter the position to move the item to: ";
	getline(cin, toStr);
	try {
		from = stringToInt(fromStr);
		to = stringToInt(toStr);
		MoveItem * itemToMove = new MoveItem(to, from, currentList);
		currentActionStack.pushActionToStack(itemToMove);
	}
	catch (exception & e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to move item" << endl;
	}
}

void undo(List& currentList, ActionTracker& currentActionStack) {
	try {
		currentActionStack.popActionFromStack(currentList);
	}
	catch (exception & e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to undo action" << endl;
	}
}

void addList(ListManager& currentListManager) {
	string listName, listDescription;
	cout << "Enter a list name: ";
	getline(cin, listName);
	cout << "Enter a list description: ";
	getline(cin, listDescription);
	try {
		AddList listToAdd(listName, listDescription, currentListManager);
	}
	catch (exception & e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to add list" << endl;
	}
}

void removeList(ListManager& currentListManager) {
	int listLocation;
	string listLocationStr;
	cout << "Enter the position of the list to delete: ";
	getline(cin, listLocationStr);
	try {
		listLocation = stringToInt(listLocationStr);
		RemoveList listToRemove(listLocation, currentListManager);
	}
	catch (exception & e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to remove list" << endl;
	}
}

void editList(ListManager& currentListManager) {
	int listLocation;
	string listLocationStr, listName, listDescription;
	cout << "Enter the position of the list to edit: ";
	getline(cin, listLocationStr);
	cout << "Enter a new list name: ";
	getline(cin, listName);
	cout << "Enter a new list description: ";
	getline(cin, listDescription);
	try {
		listLocation = stringToInt(listLocationStr);
		EditList listToEdit(listLocation, listName, listDescription, currentListManager);
	}
	catch (exception & e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to edit list" << endl;
	}
}

void moveList(ListManager& currentListManager) {
	int from, to;
	string fromStr, toStr;
	cout << "Enter the position of the list to move: ";
	getline(cin, fromStr);
	cout << "Enter the position to move the list to: ";
	getline(cin, toStr);
	try {
		from = stringToInt(fromStr);
		to = stringToInt(toStr);
		MoveList listToMove(to, from, currentListManager);
	}
	catch (exception & e) {
		std::cout << "Error: " << e.what() << endl;
		std::cout << "Unable to move item" << endl;
	}
}

List* getList(ListManager& currentListManager) {
	int listLocation;
	string listLocationStr;
	cout << "Enter the position of the list to open: ";
	getline(cin, listLocationStr);
	try {
		listLocation = stringToInt(listLocationStr);
	}
	catch (exception & e) {
		cout << "Error: " << e.what() << endl;
		cout << "Unable to remove list" << endl;
	}
	return currentListManager.getListAddress(listLocation);
}

void printList(List& currentList) {
	// Right now this only supports a description field of 64 characters
	vector<Item> allItems = currentList.getAllItems();
	cout << endl;
	cout << currentList.getName() << endl;
	cout << left << setw(10) << "Check";
	cout << left << setw(24) << "Name";
	cout << left << setw(64) << "Description";
	cout << left << setw(10) << "Quantity" << endl << endl;
	for (int k = 0; k < allItems.size(); k++) {
		Item curItem = allItems[k];
		string curName = curItem.getName();
		string curDescription = curItem.getDescription();
		string curQty = curItem.getQty();
		string curCheckString = "";
		bool curCheckBool = curItem.getCheck();
		if (curCheckBool) {
			curCheckString = "X";
		}
		cout << left << setw(10) << curCheckString;
		cout << left << setw(24) << curName;
		cout << left << setw(64) << curDescription;
		cout << left << setw(10) << curQty << endl;
	}
	cout << endl;
}

void printListManager(ListManager& currentListManager) {
	// Right now this only supports a description field of 64 characters
	vector<List> allLists = currentListManager.getAllLists();
	cout << endl;
	cout << left << setw(4) << "ID";
	cout << left << setw(24) << "Name";
	cout << left << setw(64) << "Description" << endl << endl;
	for (int k = 0; k < allLists.size(); k++) {
		List curList = allLists[k];
		string curName = curList.getName();
		string curDescription = curList.getDescription();
		cout << left << setw(4) << k;
		cout << left << setw(24) << curName;
		cout << left << setw(64) << curDescription << endl;
	}
	cout << endl;
}

int main() {
	bool programComplete = false;
	bool invalidEntry = true;
	bool itemOptionsActive = false;
	bool firstIteration = true;
	string selectionStr = "";
	int selection = 0;
	ListManager mainListManager;
	List * currentList = nullptr;
	ActionTracker actionStack;
	while (!programComplete) {
		// Print the menu
		if (itemOptionsActive) {
			// Print the updated list
			printList(*currentList);
			// Print list menu options
			printMenuItem();
			// Make sure we get a valid selection entry
			while (invalidEntry) {
				cout << "Enter a number from the list: ";
				getline(cin, selectionStr);
				try {
					selection = stringToInt(selectionStr);
				}
				catch (exception & e) {
					cout << "Error: " << e.what() << endl;
					selection = -1;
				}
				if (selection == 7) {
					invalidEntry = false;
					programComplete = true;
				}
				else if (selection < 7 && selection > -1) {
					invalidEntry = false;
				}
			}
			invalidEntry = true;
			// Choose correct action based on selection
			if (selection == 0) {
				actionStack.clearStack();
				itemOptionsActive = false;
			}
			else if (selection == 1){
				addItem(*currentList, actionStack);
			}
			else if (selection == 2) {
				removeItem(*currentList, actionStack);
			}
			else if (selection == 3) {
				editItem(*currentList, actionStack);
			}
			else if (selection == 4) {
				toggleCheckItem(*currentList, actionStack);
			}
			else if (selection == 5) {
				moveItem(*currentList, actionStack);
			}
			else if (selection == 6) {
				undo(*currentList, actionStack);
			}
		}
		else {
			if (firstIteration) {
				firstIteration = false;
			}
			else {
				// Print the updated master list
				printListManager(mainListManager);
			}
			// Print list manager menu options
			printMenuList();
			// Make sure we get a valid selection entry
			while (invalidEntry) {
				cout << "Enter a number from the list: ";
				getline(cin, selectionStr);
				try {
					selection = stringToInt(selectionStr);
				}
				catch (exception & e) {
					cout << "Error: " << e.what() << endl;
					selection = -1;
				}
				if (selection == 5) {
					invalidEntry = false;
					programComplete = true;
				}
				else if (selection < 5 && selection > -1) {
					invalidEntry = false;
				}
			}
			invalidEntry = true;
			// Choose correct action based on selection
			if (selection == 0) {
				if (mainListManager.getSizeMasterList() == 0) {
					cout << "Must have a list created in order to edit it" << endl;
					invalidEntry = true;
				}
				else {
					currentList = getList(mainListManager);
					itemOptionsActive = true;
				}
			}
			else if (selection == 1) {
				addList(mainListManager);
			}
			else if (selection == 2) {
				removeList(mainListManager);
			}
			else if (selection == 3) {
				editList(mainListManager);
			}
			else if (selection == 4) {
				moveList(mainListManager);
			}
		}
	}
}